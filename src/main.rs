use std::io;

fn main() -> io::Result<()> {
    let mut first = 2;
    let mut last = 0;

    let mut buf = String::new();

    let args: Vec<String> = std::env::args().collect();
    let input: &str = match args.get(1) {
        Some(i) => i,
        None => {
            io::stdin().read_line(&mut buf)?;

            last = 1;

            &buf[..]
        }
    };

    let input = input.replace("_", ""); // delete all underscore characters

    let radix = match &input[..2] {
        "0b" => 2,
        "0d" => 10,
        "0x" => 16,
        _ => {
            first = 0;
            16
        }
    };
    let num = u32::from_str_radix(&input[first..input.len() - last], radix).unwrap();
    println!("0b{:b}", num);
    println!("0d{}", num);
    println!("0x{:X}", num);
    Ok(())
}
