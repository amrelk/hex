### hex is a tiny tool for converting between binary, decimal, and hexadecimal
It was created out of utter frustration in the fact that the only tools to quickly
do this are on the internet, and were taking several seconds to load due to my
abysmal internet connection. It can take input from arguments, e.g.
`hex 0b10001011` or from stdin, e.g. `echo "0xa4c1" | hex`. If you think it
should have some other feature, make a merge request.